package model.post;

public class EntityExport {

    private String entityId = "46f5108d-b0b2-4f8c-a9bd-20d614e4b172";
    private boolean excludePredecessors = true;
    private boolean excludeSuccessors = true;

    public String getEntityId() {
        return entityId;
    }

    public boolean isExcludePredecessors() {
        return excludePredecessors;
    }

    public boolean isExcludeSuccessors() {
        return excludeSuccessors;
    }

    public static Builder newBuilder() {
        return new EntityExport().new Builder();
    }

    public class Builder {

        private Builder() {
        }

        public Builder setExcludePredeccessors(boolean exclude) {
            EntityExport.this.excludePredecessors = exclude;
            return this;
        }

        public Builder setExcludeSuccessors(boolean exclude) {
            EntityExport.this.excludeSuccessors = exclude;
            return this;
        }

        public Builder setEntityId(String entityId) {
            EntityExport.this.entityId = entityId;
            return this;
        }

        public EntityExport build() {
            return EntityExport.this;
        }

    }

}
