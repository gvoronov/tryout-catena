package model.get;

import java.util.Iterator;
import java.util.Map;

public class Entity {

    private String name;
    private String contentType;
    private String createdAfter;
    private String createdBefore;
    private Map<String, String> annotations;

    public Entity withName(String name) {
        this.name = name;
        return this;
    }

    public Entity withContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public Entity withCreatedAfter(String createdAfter) {
        this.createdAfter = createdAfter;
        return this;
    }

    public Entity withCreatedBefore(String createdBefore) {
        this.createdBefore = createdBefore;
        return this;
    }

    public Entity withAnnotations(Map<String, String> annotations) {
        this.annotations = annotations;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder("?");
        if (name != null) {
            b.append("name=" + name + "&");
        }
        if (contentType != null) {
            b.append("contentType=" + contentType + "&");
        }
        if (createdAfter != null) {
            b.append("createdAfter=" + createdAfter + "&");
        }
        if (createdBefore != null) {
            b.append("createdBefore=" + createdBefore + "&");
        }
        if (annotations != null) {
            Iterator iterator = annotations.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry pair = (Map.Entry) iterator.next();
                b.append("annotations." + pair.getKey() + "=" + pair.getValue() + "&");
                iterator.remove();
            }
        }
        return b.toString();
    }

}
