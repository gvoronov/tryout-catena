package domain;

public class EnvUtils {

    public static String getEnvironmentURI() {
        String host = System.getProperty("server.host");
        if (host == null) {
            return "https://tryout-catena-prov.guardtime.net";
        } else {
            return host;
        }
    }

    public static String getEnvironmentBase() {
        String base = System.getProperty("server.base");
        if (base == null) {
            return "/api/v1";
        } else {
            return base;
        }
    }

    public static String getEnvironmentUsername() {
        String username = System.getProperty("username");
        if (username == null) {
            return "ot.pD07sC";
        } else {
            return username;
        }
    }

    public static String getEnvironmentPassword() {
        String password = System.getProperty("password");
        if (password == null) {
            return "zSMw1CuN9yWm";
        } else  {
            return password;
        }
    }

}
