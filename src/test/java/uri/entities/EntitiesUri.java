package uri.entities;

import io.restassured.response.Response;
import model.get.Entity;
import model.post.EntityExport;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

public class EntitiesUri {

    private static final String uri = "/entities";
    private static final String uriExport = uri + "/export";

    public static Response getEntities() {
        return when().get(uri);
    }

    public static Response getEntity(Entity entity) {
        return when().get(uri + entity.toString());
    }

    public static Response getEntityById(String id) {
        return when().get(uri + "/" + id);
    }

    public static Response getEntityByName(String name) {
        return when().get(uri + "?name=" + name);
    }

    public static Response getEntityExportById(String id) {
        return when().get(uriExport + "/" + id);
    }

    public static Response postEntityExport(EntityExport entity) {
        return given()
                .contentType("application/json")
                .body(entity)
                .when()
                .post(uriExport);
    }

}
