package tests;

import domain.EnvUtils;
import model.post.EntityExport;
import org.testng.annotations.Test;
import uri.entities.EntitiesUri;

import static org.hamcrest.Matchers.equalTo;

@Test
public class EntitiesExportTest extends BaseTest {

    @Test(description = "Create Export Request, Verify that it can be reached by ID")
    public void postEntityExport() {
        EntityExport entity = EntityExport.newBuilder()
                .setEntityId("77d9829d-921d-4a53-a7c7-920423895acb")
                .build();

        String exportId = EntitiesUri
                .postEntityExport(entity)
                .then()
                .body("createdBy", equalTo(EnvUtils.getEnvironmentUsername().toLowerCase()))
                .statusCode(200)
                .extract().path("id");

        EntitiesUri
                .getEntityExportById(exportId)
                .then()
                .statusCode(200);
    }

}
