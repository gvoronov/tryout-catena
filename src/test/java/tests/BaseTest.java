package tests;

import domain.EnvUtils;
import io.restassured.RestAssured;
import org.testng.annotations.BeforeSuite;

import static io.restassured.RestAssured.basic;

public abstract class BaseTest {

    @BeforeSuite
    public void setup() {
        RestAssured.baseURI = EnvUtils.getEnvironmentURI();
        RestAssured.basePath = EnvUtils.getEnvironmentBase();
        RestAssured.authentication = basic(EnvUtils.getEnvironmentUsername(), EnvUtils.getEnvironmentPassword());
    }

}
