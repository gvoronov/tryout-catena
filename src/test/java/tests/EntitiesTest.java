package tests;

import model.get.Entity;
import org.testng.annotations.Test;
import uri.entities.EntitiesUri;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;

@Test
public class EntitiesTest extends BaseTest {

    @Test(description = "Ping Entity by valid ID, Verify the response code")
    public void pingEntityById() {
        EntitiesUri
                .getEntityById("46f5108d-b0b2-4f8c-a9bd-20d614e4b172")
                .then()
                .statusCode(200);
    }

    @Test(description = "Ping Entity by valid Name, Verify the response code")
    public void pingEntityByName() {
        EntitiesUri
                .getEntityByName("Homo")
                .then()
                .statusCode(200);
    }

    @Test(description = "Ping Entity by invalid ID, Verify the response code")
    public void invalidEntityId() {
        EntitiesUri
                .getEntityById("aaa111")
                .then()
                .statusCode(404);
    }

    @Test(description = "Entity request without parameters, Verify the response code")
    public void invalidEntityRequest() {
        EntitiesUri
                .getEntities()
                .then()
                .statusCode(400);
    }

    @Test(description = "Request Entity, Verify it has correct values")
    public void entityValues() {
        EntitiesUri
                .getEntityById("46f5108d-b0b2-4f8c-a9bd-20d614e4b172")
                .then()
                .body("name", equalTo("Homininae"))
                .body("createdBy.id", equalTo("guardtime"));
    }

    @Test(description = "Request Entities by detailed query, Verify the Response has the requested item")
    public void entityDetailedQuery() {
        Map<String, String> map = new HashMap<>();
        map.put("taxonomy id", "9605");
        map.put("parent taxonomy id", "207598");
        Entity entity = new Entity()
                .withName("Homo")
                .withContentType("text/plain")
                .withAnnotations(map);

        EntitiesUri
                .getEntity(entity)
                .then()
                .body("ids", hasItem("77d9829d-921d-4a53-a7c7-920423895acb"))
                .statusCode(200);
    }

}
